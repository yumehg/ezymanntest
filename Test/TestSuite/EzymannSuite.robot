*** Settings ***
Documentation   Testing Ezyman Suite
Library         Selenium2Library
Library         BuiltIn
Resource        ../Keyword/Common.robot
Resource        ../Keyword/LoginKeywords.robot

*** Test Case ***
TestCase1: Login Correct Case
    [Documentation]    Login Correct Username and Password Case
    [Tags]    normal
    [Setup]
        Open Ezymann Website in Chrome and Maximize
        Provide Username and Password
    [Teardown]    Close Browser

TestCase2: Login Incorrect Username Case
    [Documentation]    Login Incorrect Username Case
    [Tags]    wrong
    [Setup]
        Open Ezymann Website in Chrome and Maximize
        Provide Username
    [Teardown]    Close Browser

TestCase3: Login Incorrect Password Case
    [Documentation]    Login Incorrect Password Case
    [Tags]    wrong
    [Setup]
        Open Ezymann Website in Chrome and Maximize
        Provide Password
    [Teardown]    Close Browser