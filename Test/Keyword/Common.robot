*** Settings ***
Documentation   Testing Ezyman Keywords
Library         Selenium2Library
Library         BuiltIn
Variables       ../Variable/Variables.py

*** Keywords ***
Open Ezymann Website in Chrome and Maximize
    Open Browser    url=${URL}    browser=chrome
    Maximize Browser Window
    #Log Browser to Maximized
    Log to console    Browser is Maximized