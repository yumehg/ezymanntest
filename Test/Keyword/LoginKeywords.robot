*** Settings ***
Documentation   Testing Ezyman Keywords
Library         Selenium2Library
Library         BuiltIn
Variables       ../Variable/Variables.py
Variables       ../Locators/Elements.py

*** Keywords ***
Provide Username and Password
    Input Text    ${Username_name}    ${Username}
    Input Text    ${Password_name}    ${Password}
    Submit Form
    Sleep    ${timeout}
    #Log
    Log to console    Entered Username
    Log to console    Entered Password
    Log to console    Login Success

Provide Username
    Input Text    ${Username_name}    ${Username}
    Submit Form
    Sleep    ${timeout}
    #Log
    Log to console    Entered Username
    Log to console    Login Fail

Provide Password
    Input Text    ${Password_name}    ${Password}
    Submit Form
    Sleep    ${timeout}
    #Log
    Log to console    Entered Password
    Log to console    Login Fail
